//
//  ViewController.swift
//  FaceRecognition
//
//  Created by Buwaneka De Silva on 7/28/18.
//  Copyright © 2018 Buwaneka De Silva. All rights reserved.
//

import UIKit
import Vision

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        guard let image = UIImage(named: "sample")else{
            return
        }
        
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        let scaledHeight = view.frame.width / image.size.height * image.size.height
        imageView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: scaledHeight)
    
        view.addSubview(imageView)
        
        let request = VNDetectFaceRectanglesRequest { (req, err) in
            if let err = err{
                print("Fail to detect faces", err)
                return
            }

            req.results?.forEach({ (res) in
                print(res)
                
                guard let faceObservation = res as? VNFaceObservation else{return}
                let height = scaledHeight * faceObservation.boundingBox.height
                let x = self.view.frame.width * faceObservation.boundingBox.origin.x
                let y = self.view.frame.width * (1 - faceObservation.boundingBox.origin.y) - height
                let width = self.view.frame.width * faceObservation.boundingBox.width

                
                let redview = UIView()
                redview.backgroundColor = .red
                redview.alpha = 0.4
                redview.frame = CGRect(x: x, y: y, width: width, height: height)
                self.view.addSubview(redview)
                
                print(faceObservation.boundingBox)
            })
        }
        
        guard let cgImage = image.cgImage else {return}
        let handler = VNImageRequestHandler(cgImage: cgImage, options: [:])
        
        do{
           try handler.perform([request])
        }catch let reqErr {
            print ("Failed to perform request ", reqErr)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

